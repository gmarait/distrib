Statut de notre système d'intégration continue.
** SCOTCH
   - [[https://gitlab.inria.fr/scotch/scotch/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/scotch/show][scotch]]
     - linux ubuntu, 32 et 64bits
** HQR
   - [[https://gitlab.inria.fr/solverstack/hqr/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/morse/show][morse]]
     - image docker ubuntu 20.04
     - macosx catalina
** SPM
   - [[https://gitlab.inria.fr/solverstack/spm/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/morse/show][morse]]
     - image docker ubuntu 20.04
** CHAMELEON
   - [[https://gitlab.inria.fr/solverstack/chameleon/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/morse/show][morse]]
     - image docker ubuntu 20.04
     - macosx catalina
** PASTIX
   - [[https://gitlab.inria.fr/solverstack/pastix/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/morse/show][morse]]
     - image docker ubuntu 20.04
** QRMUMPS
   - [[https://gitlab.com/qr_mumps/qr_mumps/-/pipelines][gitlab.com]]
   - shared runner gitlab.com
     - image docker abuttari/qrm_ci:0.2
** FABULOUS
   - [[https://gitlab.inria.fr/solverstack/fabulous/-/pipelines][gitlab inria]]
   - machine guix.bordeaux.inria.fr
** MAPHYS (++)
   - [[https://gitlab.inria.fr/solverstack/maphys/maphyspp/-/pipelines][gitlab inria]]
   - machine guix.bordeaux.inria.fr
** SCALFMM
   - [[https://gitlab.inria.fr/solverstack/ScalFMM/-/pipelines][gitlab inria]]
   - vm ci.inria.fr projet [[https://ci.inria.fr/project/morse/show][morse]]
     - image docker ubuntu 20.04
** Problématiques
   Il y a deux grandes problématiques à résoudre:
   1) Tester un solveur spécifique, numériquement, dans un ensemble de
      configurations (avec/sans mpi/cuda, choix runtime, int32/64,
      etc). Pour être raisonnable en terme de nombre de jobs un
      environnement logiciel unique peut être fixé. Il est
      certainement spécifique au solveur. Il est, au choix des
      développeurs, déployé sur une VM ou une machine spécifique (ex
      une machine guix OS avec des cartes Nvidia, plafrim), avec
      docker ou guix ou même en natif (selon culture du développeur
      qui maintient sa CI). La recette pour le déploiement de
      l'environnement de test est documentée.
   2) Tester la distribution logicielle. C'est à dire la capacité du
      solveur à s'installer dans différentes configurations
      logicielles. On voudrait valider ce déploiement avec GNU Guix
      qui dispose des paquets dont on a besoin et qui est l'outil le
      plus robuste en terme de reproductibilité de l'installation.

   Dans l'idéal on cherche à tester l'ensemble de cas de la matrice de
   configuration telle que définie dans la section suivante.

** Configurations
   Ceci est une proposition des configurations à envisager. La liste
   n'est pas exhaustive. Elle doit être affinée collectivement par les
   membres de DISTRIB.
*** architecture
    1. Intel
    2. Nvidia
    3. AMD
    4. IBM
*** système d'exploitation
    1. Linux Ubuntu
    2. Linux Centos
    3. Linux Guix OS
    4. MacOSX
    5. Windows
*** compilateur
    1. GNU GCC
    2. Clang
    3. Intel ICC
    4. AMD AOCC
    5. IBM XLC/C++
*** blas/lapack
    1. Netlib BLAS (ref)
    2. OpenBLAS
    3. Intel MKL
    4. AMD BLIS/FLAME
    5. IBM ESSL
*** mpi
    1. OpenMPI
    2. MPICH
    3. Intel MPI
    4. Nmad
*** partitionneur
    1. Scotch/PT-Scotch
    2. Metis/Parmetis
*** runtime
    1. Starpu
    2. Parsec
    3. OpenMP

** Configuration(s) pour validation numérique de chaque solveur
   Dans les faits difficile de tester toutes les combinaisons pour les
   points 1) et 2) décrits précédemment.

   En revanche il serait intéressant de définir la ou les combinaisons
   à utiliser pour 1), /i.e./ validation numérique du solveur, ceci pour
   chaque bibliothèque solveur. Ensuite définir les combinaisons pour
   2), /i.e./ validation de la distribution via GNU Guix, cette
   définition étant commune pour plus de cohérence cf. section
   suivante.

   #+CAPTION: Configuration de CI pour validation numérique
   #+ATTR_HTML: :align center
   | Library   | Arch         | OS                  | Compilo | Blas            | MPI          |
   |-----------+--------------+---------------------+---------+-----------------+--------------|
   | Scotch    | Intel        | Ubuntu              | GCC     | -               | OpenMPI      |
   | StarPU    | Intel/Nvidia | Linux/MacOS/Windows | GCC     | -               | OpenMPI/Nmad |
   | Chameleon | Intel        | Ubuntu/MacOS X      | GCC     | Netlib/OpenBLAS | OpenMPI      |
   | PaStiX    | Intel        | Ubuntu              | GCC     | Netlib          | OpenMPI      |
   | qrmumps   | Intel        | Ubuntu              | GCC     | OpenBlas        | -            |
   | MaPHyS    | Intel        | Guix OS             | GCC     | OpenBlas        | OpenMPI      |
   | ScalFMM   | Intel        | Guix OS             | GCC     | Netlib          | OpenMPI      |

   Notes sur les runtimes:
   - chameleon est testé avec 4 runtimes différents: starpu, parsec,
     openmp, quark
   - pastix est testé avec static, parsec et starpu
   - qr_mumps est testé avec starpu
   - scalfmm pas encore testé avec openmp et starpu

   Note sur les partitionneurs de graph:
   - pastix testé avec scotch (32) et metis (32)
   - qr_mumps testé avec amd (suite-sparse), metis (32) et scotch (32)
   - maphys++ pas de dépendance sur partitionneur car gestion DDM
     externalisée (doit être opérée avant)

** Configurations pour validation de la distribution GNU Guix
   #+CAPTION: Configuration GNU Guix pour validation de la distribution
   #+ATTR_HTML: :align center
   | Library   |      Version | Arch         | Compilo | Blas         | MPI          |
   |-----------+--------------+--------------+---------+--------------+--------------|
   | Scotch    |        6.1.1 | Intel        | GCC     | -            | OpenMPI/Nmad |
   | PaRSEC    |     6022a61d | Intel        | GCC     | -            | OpenMPI      |
   | StarPU    |        1.3.8 | Intel/Nvidia | GCC     | -            | OpenMPI/Nmad |
   | Chameleon |        1.1.0 | Intel/Nvidia | GCC     | OpenBLAS/MKL | OpenMPI/Nmad |
   | PaStiX    |        6.2.1 | Intel/Nvidia | GCC     | OpenBLAS/MKL | OpenMPI      |
   | qrmumps   |        3.0.4 | Intel/Nvidia | GCC     | OpenBLAS/MKL | -            |
   | Fabulous  |        1.1.2 | Intel        | GCC     | OpenBLAS     | -            |
   | MaPHySf90 |        1.0.0 | Intel        | GCC     | OpenBLAS     | OpenMPI/Nmad |
   | MaPHys++  |        1.0.0 | Intel        | GCC     | OpenBLAS     | OpenMPI      |
   | ScalFMM   | experimental | Intel        | GCC     | OpenBLAS/MKL | OpenMPI      |

   Notes sur les runtimes:
   - chameleon peut utiliser au choix 4 runtimes différents: starpu,
     parsec (sans MPI), openmp, quark
   - pastix est compilé avec parsec (avec MPI) et starpu
   - qrmumps compilé avec starpu
   - parsec ne peut pas utiliser Nmad pour le moment (manque des
     symbole, ex: MPI_Comm_set_info)

   Note sur les partitionneurs de graph:
   - pastix compilé avec scotch (64)
   - qrmumps avec scotch32 et metis
   - maphysf90 avec scotch
   - maphys++ pas de dépendance sur partitionneur car gestion DDM
     externalisée (doit être opérée avant)

   Manque aujourd'hui:
   - releases de scalfmm 3
   - intégration continue pour toutes les versions non-free (mkl et
     cuda notamment)
     - maphys++ avec mkl, ajouter test avec
       --with-input=mumps-openmpi=mumps-mkl-openmpi
       --with-input=openblas=mkl
     - scalfmm avec cuda
   - une cohérence dans les paquets blas/lapack dans guix, certains
     openblas d'autres netlib ... voir mumps et petsc en point de
     départ
   - paquet openblas avec threads activés
   - solveurs linkés avec mkl-mt (seul chameleon est ok)
   - une cohérence dans les paquets mpi dans guix (ex: openmpi par
     défaut ?)
