#!/bin/sh
# whatis: standard development environment to use the fabulous library

sudo apt-get update -y
sudo apt-get install build-essential git gfortran cmake doxygen python pkg-config libopenblas-dev liblapacke-dev -y
# libopenblas-dev can be replaced by libmkl-dev or liblapack-dev

# install fabulous with cmake
git clone --recursive https://gitlab.inria.fr/solverstack/fabulous.git
cd fabulous && cmake -S . -B build -DFABULOUS_BUILD_C_API=ON -DFABULOUS_BUILD_Fortran_API=ON -DFABULOUS_BUILD_DOC=ON -DFABULOUS_BUILD_EXAMPLES=ON -DBUILD_SHARED_LIBS=ON && cmake --build build -j5 && sudo cmake --install build

# example usage: use fabulous library in your own cmake project (we provide a FabulousConfig.cmake)
cd ..
git clone https://gitlab.inria.fr/solverstack/distrib.git
cd distrib/cmake/test/fabulous && mkdir build && cd build && cmake .. && make && ./test_fabulous_cpp

# example usage: use fabulous library in your own not cmake project
# use pkg-config to get compiler flags and linking
pkg-config --cflags fabulous_cpp
pkg-config --libs fabulous_cpp
# if there are static libraries use the --static option of pkg-config
