FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y
RUN apt-get install git build-essential wget tar curl devscripts dh-make quilt pbuilder sbuild lintian svn-buildpackage git-buildpackage -y
RUN apt-get install gfortran cmake pkg-config libopenblas-dev liblapacke-dev -y

ARG GITLABTOKEN
ARG GITLABPROJNUM=2083
ARG DEBIANDIST=ubuntu_20.04
ARG DEBIANARCH="1_amd64"
ARG RELEASEVER="1.1.2"
ARG RELEASEURL=https://gitlab.inria.fr/solverstack/fabulous/uploads/e972b50d111410b9f3c2e0e982f145f0/fabulous-1.1.2.tar.gz
ARG RELEASETAR=fabulous-$RELEASEVER.tar.gz
ARG RELEASETARDEB=fabulous_$RELEASEVER.orig.tar.gz

RUN wget $RELEASEURL
RUN mv $RELEASETAR $RELEASETARDEB
RUN tar xvf $RELEASETARDEB

ENV LOGNAME=root
ENV DEBEMAIL=florent.pruvost@inria.fr
RUN cd /fabulous-$RELEASEVER/ && dh_make --library --yes

COPY changelog /fabulous-$RELEASEVER/debian/
COPY control /fabulous-$RELEASEVER/debian/
COPY copyright /fabulous-$RELEASEVER/debian/
COPY rules /fabulous-$RELEASEVER/debian/

RUN export DEB_BUILD_OPTIONS='nocheck' && cd /fabulous-$RELEASEVER/ && debuild -us -uc

RUN apt-get install ./fabulous_$RELEASEVER-$DEBIANARCH.deb -y
RUN /usr/lib/fabulous/examples/example_api

RUN git clone https://gitlab.inria.fr/solverstack/distrib.git
RUN cd /distrib/cmake/test/fabulous && mkdir build && cd build && cmake .. && make && ./test_fabulous_cpp

RUN curl --header "PRIVATE-TOKEN: ${GITLABTOKEN}" --upload-file ./fabulous_$RELEASEVER-$DEBIANARCH.deb "https://gitlab.inria.fr/api/v4/projects/$GITLABPROJNUM/packages/generic/$DEBIANDIST/$RELEASEVER/fabulous_$RELEASEVER-$DEBIANARCH.deb"
