#!/bin/sh
# whatis: standard development environment to use the qrmumps library

sudo apt-get update -y
sudo apt-get install build-essential git gfortran cmake emacs perl pkg-config wget libopenblas-dev libstarpu-dev libscotch-dev libmetis-dev libsuitesparse-dev -y
# libopenblas-dev can be replaced by libmkl-dev or liblapack-dev

# install qrmumps with cmake
wget http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-3.0.3.tgz
tar xvf qr_mumps-3.0.3.tgz
cd qr_mumps-3.0.3
cmake -S . -B build -DQRM_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON && cmake --build build -j5 && sudo cmake --install build

# TODO: wait for next release with the qrmConfig.cmake to be able to get link target in a cmake project using qr_mumps
