\documentclass[slideopt,A4,showboxes,svgnames]{beamer}


%% list of packages here
\usepackage{multirow}

\renewcommand{\sfdefault}{lmss}
\sffamily

\setbeamersize{text margin left=1cm,text margin right=1cm}
\setbeamerfont{alerted text}{series=\bfseries}
\setbeamerfont{example text}{series=\bfseries}

\usepackage[absolute,showboxes,overlay]{textpos}

\TPshowboxesfalse
\textblockorigin{0mm}{0mm}

\hypersetup{
  allcolors=rouge_inria,
}

\newcommand{\GRAND}{\fontsize{100}{100}\selectfont}
\newcommand{\Grand}{\fontsize{80}{80}\selectfont}
\newcommand{\grand}{\fontsize{60}{60}\selectfont}

%% Un point rouge simple sans ombré pour les puces : OK
\title[HiePACS Solver Distribution]{HiePACS Solver\\ Distribution}
\subtitle{Define a common target}
\date[18/09/2020]{18/09/2020}
\author[Florent Pruvost]{}

\usetheme{inria}
%\usetheme{inria2}
%\usetheme{inria3}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Titre de la présentation avec format Inria
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}
  \titlepage
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%Plan de la présentation

%Chapitre 1
\frame{\tocpage}

\section{History}
\frame{\sectionpage}

% - Morse distrib 1.0 CMake, gforge inria (svn)
% - Refonte des cmake séparés, système de détection commun
% - système de distribution Spack, intégration continue
% - discussions sur API commune non concluante
% - migration vers gitlab, svn -> git, jenkins -> gitlab-ci
% (image docker)
% - instabilités: réécriture des libs, changement de nom (Chameleon),
% changement de dépôt (gitlab), changement de dépendances (magma,
% simgrid, parsec, etc)

\begin{frame}{[2012] CMake Morse Prototype}
  \textbf{Project: HiePACS/SED - Cédric Castagnède}
\end{frame}

\begin{frame}{[2012] CMake Morse Prototype}

  \begin{center}
    \only<1>{
      \textbf{Archives on GForge Inria}\\
      \includegraphics[width=\linewidth]{Figs/distrib_forge_morse.png}
    }
    \only<2>{
      \textbf{MagmaMorse (ex Chameleon), PaStiX, ScalFMM}\\
      \includegraphics[width=0.3\linewidth]{Figs/proto_morse_folder.png}
    }
    \only<3>{
      \textbf{Based on CMake options, macros, common detection
        system}\\
      \includegraphics[width=0.6\linewidth]{Figs/distrib_cmake_morse.png}
    }
  \end{center}

  \only<4>{
    \begin{exampleblock}{Features}
      \begin{itemize}
      \item many options:
        \begin{itemize}
        \item precisions (single, double, complex, mix, ...)
        \item runtimes (Quark, StarPU), MPI/CUDA
        \item kernels (BLAS/LAPACK, cuBLAS)
        \end{itemize}
      \item dependencies management:
        \begin{itemize}
        \item user specific: precise local paths are given
        \item automatic: search within the system, install/download tarball
        \end{itemize}
      \end{itemize}
    \end{exampleblock}

    \begin{alertblock}{So, what's wrong ?}
      \begin{itemize}
      \item many ad-hoc CMake macros/functions\\ $\rightarrow$ difficult
        to debug and maintain for researchers
      \item CMake is a build/install system, not a distribution system
      \end{itemize}
    \end{alertblock}
  }

\end{frame}


\begin{frame}{[2014-2016] HPC Collective}
  \textbf{Project: ADT HPC Collective - Florent Pruvost}
\end{frame}

\begin{frame}[fragile]{[2014-2016] CMake base}

  \begin{exampleblock}{\centering Rely on a solid build-install process}
    \begin{itemize}
    \item share common CMake practices
    \item rely on the same detection of the system and libraries
    \item recursive system of CMake Finds
      {\scriptsize
\begin{verbatim}
  find_package(CHAMELEON COMPONENTS STARPU MPI CUDA FXT)
  --> CHAMELEON_LIBRARIES_DEP, CHAMELEON_INCLUDE_DIRS_DEP, ...
\end{verbatim}
      }
    \end{itemize}
  \end{exampleblock}

  List of available \texttt{find\_package} in Morse:
  \begin{center}
    {\scriptsize
      \begin{tabular}{ll}
        \hline
        ext. solvers & hypre, petsc, plasma, slepc, scalapack \\
        int. solvers & chameleon, fabulous, hqr, maphys, mumps, pastix \\
        runtimes & quark, parsec, starpu \\
        kernels  & (c)blas, lapack(e), fftw \\
        graphs   & (par)metis, (pt)scotch, pampa \\
        misc     & hwloc, fxt, eztrace, simgrid, spm \\\hline
      \end{tabular}
    }
  \end{center}
  \alert{Available at: \url{https://gitlab.inria.fr/solverstack/morse\_cmake}}

\end{frame}

\begin{frame}[fragile]{[2014-2016] Spack distribution}
  \begin{exampleblock}{Install solvers + dependencies - Requirements}
    \begin{itemize}
    \item A simple process to install a default version
    \item A flexible way to choose build variants
      \begin{itemize}
      \item {\small choose compiler, software versions (releases, devs. branches)}
      \item {\small change components/options, \textit{e.g.} w/o mpi, \texttt{--enable-debug}}
      \end{itemize}
    \item Be able to install it on a remote cluster
      \begin{itemize}
      \item {\small no root permissions, no internet access}
      \end{itemize}
    \end{itemize}
  \end{exampleblock}

  \begin{exampleblock}{Spack solution (Linux, MacOS)}
    {\scriptsize
\begin{verbatim}
  git clone https://github.com/llnl/spack.git
  git clone https://gitlab.inria.fr/solverstack/spack-repo.git
  ./spack/bin/spack repo add $PWD/spack-repo
  spack install chameleon@master~simgrid+mpi+starpu %gcc ^openblas ^openmpi
\end{verbatim}
    }
  \end{exampleblock}

\end{frame}


\begin{frame}[fragile]{[2014-2016] Spack distribution}

  \textbf{CI pipeline:} test installation of default versions (releases)\\
  \includegraphics[width=\linewidth]{Figs/spack-repo-pipeline.png}
  \alert{Available at:
    \url{https://gitlab.inria.fr/solverstack/spack-repo}}

\end{frame}

\begin{frame}{[2016-2018] HPClib}
  \textbf{Project: ADT HPClib - \\
    Florent Pruvost + Mathieu Faverge + Gilles Marait + ...}
  ~\\
  ~\\
  \begin{itemize}
  \item migration from gforge to gitlab inria
  \item continuous integration with gitlab-ci
  \item shared testing environment : same virtual machines and docker
    image \textit{hpclib/hiepacs}
  \item code quality analysis, SonarQube
  \end{itemize}
\end{frame}

\begin{frame}{[2018-2020] ADTs Guix-HPC, Gordon, ...}

  %\only<1>{
    \textbf{Project: ADT Guix HPC - Ludovic Courtès}\\
    \textbf{Project: ADT Gordon - Florent Pruvost}

    \begin{itemize}
    \item improve GNU Guix package manager to make it available in a HPC
      context
    \item develop HiePACS packages
    \item reproducible experiments: ADT Gordon, others
    \item Gitlab-CI:
      \begin{itemize}
      \item testing environmnent (Maphys),
      \item usage to monitor performances on PlaFRIM (Chameleon, PaStiX,
        Maphys++)
      \end{itemize}
    \end{itemize}
  %}
  %\only<2>{
  %  Chameleon performances are tested each week with a pipeline
  %  gitlab-ci + guix + jube, elasticsearch db, display on kibana\\
  %  \begin{center}
  %    \includegraphics[width=\linewidth]{Figs/chameleon-kibana.png}
  %  \end{center}
  %  \alert{Available at: \url{https://kibana.bordeaux.inria.fr}}
  %}

\end{frame}

\begin{frame}{[2018-2020] ADTs Guix-HPC, Gordon, ...}

  \textbf{CI pipeline:} test installation of default versions (releases)\\
  \begin{center}
    \includegraphics[width=0.5\linewidth]{Figs/guix-hpc-pipeline.png}
  \end{center}

  \alert{Available at:
    \url{https://gitlab.inria.fr/guix-hpc/guix-hpc}}

\end{frame}

%Chapitre 2
\section{Priorities}
\frame{\sectionpage}


\begin{frame}{Two directions}

  \begin{exampleblock}{Extern}
    \begin{itemize}
    \item stabilization of well identified versions (release), highly tested
    \item complete installation instructions
    \item common pretty website
    \item pre-built binary packages ?
    \end{itemize}
  \end{exampleblock}

  \begin{exampleblock}{Intern}
    \begin{itemize}
    \item give a recipe and environment in which development versions
      are guaranteed to work well
    \item work on several combinations:
      \begin{itemize}
      \item different branches/releases
      \item compilers, Blas/Lapack, MPI, and other build variants
      \end{itemize}
    \item rely on Guix-HPC packages and gitlab-ci pipelines
    \end{itemize}
  \end{exampleblock}

\end{frame}

%Chapitre 3

\section{Proposal}
\frame{\sectionpage}

\begin{frame}{Build the missing showcase}

  \begin{enumerate}
  \item Tackle what is missing, i.e. the showcase
    \begin{itemize}
    \item to get something visible from the exterior, easier to point to
    \item to ease discussions with our managers
    \item initial target: \textit{Chameleon, MaPHyS, PaStiX, ScalFMM}
    \item secondary target: \textit{Scotch, StarPU}
    \end{itemize}
  \item Composability with Guix can be done with real-time updates
    \begin{itemize}
    \item L. Courtès from the SED is already working on Guix-HPC
    \item there are many engineers in the team that can work on
      improving/testing each solver stack
    \item we will add new builds combinations in our CI process
    \end{itemize}
  \end{enumerate}

\end{frame}

\begin{frame}{Tasks and Schedule proposal}
  \begin{itemize}
  \only<1>{
  \item[1.] \textbf{[12/2020] Modern CMake update}
    \begin{itemize}
    \item define targets in \textit{Finds} (detection system)
    \item use properties on targets in CMake projects instead of lists
    \item export our targets at installation
    \item documentation update: detection system, cmake usage and
      options in each project
    \end{itemize}
  \item[2.] \textbf{[03/2021] Continuous Integration update}
    \begin{itemize}
    \item more tests in different environments (Linux/MacOS/Guix)
    \item test variants GCC/Clang/Intel, OpenBLAS/Intel MKL,
      OpenMPI/MPICH
    \item documentation about environment settings
    \end{itemize}
  }
  \only<2>{
  \item[3.] \textbf{[06/2021] Build Releases}
    \begin{itemize}
    \item check the documentations (install and use)
    \item share common release methodology (scripts, branching) ?
    \item provide pre-built binary packages (.deb, .pkg, etc) ?
    \end{itemize}
  \item[4.] \textbf{[09/2021] Build a common nice looking website}
    \begin{itemize}
    \item news, articles, links to releases, git projects, performances
    \end{itemize}
  }
  \end{itemize}

\end{frame}

\begin{frame}{Ressources and Governance}

  \begin{exampleblock}{Ressources}
    \begin{itemize}
    \item SED: FP 20\%
    \item HiePACS solver maintainers: engineers / researchers
    \item Website: comm. department, subcontract, us ?
    \end{itemize}
  \end{exampleblock}

  \begin{exampleblock}{Governance}
    \begin{itemize}
    \item Scientific supervisor: Emmanuel Agullo
    \item Technical supervisor: Mathieu Faverge
    \end{itemize}
  \end{exampleblock}

\end{frame}

\begin{frame}{Project name proposal !}

  \begin{center}
    \huge{\textcolor{rouge_inria}{DISTRIB}}\\
    \normalsize{
    \begin{center}
      \includegraphics[width=0.1\linewidth]{Figs/distrib_logo.png}
    \end{center}
    \textcolor{rouge_inria}{D}\textcolor{gray}{irect}
    \textcolor{gray}{and}
    \textcolor{rouge_inria}{I}\textcolor{gray}{terative}
    \textcolor{rouge_inria}{S}\textcolor{gray}{olvers}
    \textcolor{gray}{on}
    \textcolor{rouge_inria}{T}\textcolor{gray}{op}
    \textcolor{gray}{of}
    \textcolor{rouge_inria}{R}\textcolor{gray}{untimes} \textcolor{gray}{at} \textcolor{rouge_inria}{I}\textcolor{gray}{nria} \textcolor{rouge_inria}{B}\textcolor{gray}{ordeaux}}
  \end{center}

\end{frame}

\end{document}
