;; publish.el
;; Emacs publish file for project.
;; Run the following command to execute:
;; emacs --batch --no-init-file --load publish.el --funcall org-publish-all

;; Packages:
(require 'package)
(require 'ox-publish)
(require 'ox-extra)
(require 'org)
(require 'org-ref)
(require 'htmlize)

(setq org-src-fontify-natively t)
;; Source code block configuration:
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (C . t)
   (fortran . t)
   (python . t)
   (shell . t)
  ))

(setq org-publish-project-alist
      `(("website"
         :base-directory "."
         :base-extension "org"
         :publishing-directory "."
         :publishing-function org-html-publish-to-html
         )))
