#!/bin/sh
git clone https://bitbucket.org/mfaverge/parsec.git
cd parsec
git checkout mymaster
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -DPARSEC_GPU_WITH_CUDA=OFF -DPARSEC_DIST_WITH_MPI=ON
make -j5 install
cd ../..
