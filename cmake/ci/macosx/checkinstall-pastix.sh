#!/bin/sh
export PASTIX_ROOT=$PWD/pastix-6.2.1/build/install
cd cmake/test/pastix
mkdir build
cd build
cmake .. -DBLA_PREFER_PKGCONFIG=ON
make
./test_pastix --lap 100
