#!/bin/sh

# install starpu (may be missing)
brew install --build-from-source ~/brew-repo/starpu.rb

# install chameleon
wget https://gitlab.inria.fr/solverstack/chameleon/uploads/b299d6037d7636c6be16108c89bc2aab/chameleon-1.1.0.tar.gz
tar xvf chameleon-1.1.0.tar.gz
cd chameleon-1.1.0
mkdir build && cd build
cmake .. -DCHAMELEON_USE_MPI=ON -DBLA_PREFER_PKGCONFIG=ON -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j5 install
cd ../..
