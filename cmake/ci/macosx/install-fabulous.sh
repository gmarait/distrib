#!/bin/sh

if brew ls --versions fabulous > /dev/null; then brew remove --force --ignore-dependencies fabulous; fi

wget https://gitlab.inria.fr/solverstack/fabulous/uploads/e972b50d111410b9f3c2e0e982f145f0/fabulous-1.1.2.tar.gz
tar xvf fabulous-1.1.2.tar.gz
cd fabulous-1.1.2
mkdir build && cd build
cmake .. -DFABULOUS_BUILD_C_API=ON -DFABULOUS_BUILD_Fortran_API=ON -DFABULOUS_BUILD_EXAMPLES=ON -DFABULOUS_BUILD_TESTS=OFF -DBLA_PREFER_PKGCONFIG=ON -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j2 install
cd ../..
