#!/bin/sh
export QRM_ROOT=$PWD/qr_mumps-3.0.4/build/install
cd cmake/test/qrmumps
mkdir build
cd build
cmake .. -DBLA_PREFER_PKGCONFIG=ON
make
# ./sqrm_least_squares_full
