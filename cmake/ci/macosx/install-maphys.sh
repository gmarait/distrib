#!/bin/sh

# install blaspp lapackpp arpack-ng and maphyspp with cmake
# This can be done only occasionally directly on the vm to update versions of blaspp, lapackpp and arpack-ng
# BLAS_LIBRARIES="-DBLAS_LIBRARIES=-L/usr/local/Cellar/openblas/0.3.17/lib -lopenblas"
# LAPACK_LIBRARIES="-DLAPACK_LIBRARIES=-L/usr/local/Cellar/openblas/0.3.17/lib -lopenblas"
# git clone https://bitbucket.org/icl/blaspp.git
# git clone https://bitbucket.org/icl/lapackpp.git
# git clone https://github.com/opencollab/arpack-ng.git
#
# cd blaspp
# mkdir build && cd build
# cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local $BLAS_LIBRARIES
# make -j3 install
# cd ../..
#
# cd lapackpp
# mkdir build && cd build
# cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local $LAPACK_LIBRARIES
# make -j3 install
# cd ../..
#
# cd arpack-ng
# mkdir build && cd build
# cmake .. -DICB=ON $BLAS_LIBRARIES $LAPACK_LIBRARIES
# make -j3 install
# cd ../..

# install pastix
brew install --build-from-source ~/brew-repo/scotch.rb
brew install --build-from-source ~/brew-repo/pastix.rb

# official mumps distribution on macosx not found
# TODO: find one and enable mumps for maphys

# install maphys
wget https://gitlab.inria.fr/solverstack/maphys/maphyspp/uploads/363105a63cc490083de7bc1648001990/maphyspp-1.1.0.tar.gz
tar xvf maphyspp-1.1.0.tar.gz
cd maphyspp-1.1.0
mkdir build && cd build
cmake .. -DMAPHYSPP_COMPILE_EXAMPLES=OFF -DMAPHYSPP_COMPILE_TESTS=OFF -DMAPHYSPP_USE_MUMPS=OFF -DMAPHYSPP_USE_PASTIX=ON -DBUILD_SHARED_LIBS=ON -DBLA_PREFER_PKGCONFIG=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j2 install
cd ../..
