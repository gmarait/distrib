#!/bin/sh
export Fabulous_ROOT=$PWD/fabulous-1.1.2/build/install
cd cmake/test/fabulous
mkdir build
cd build
cmake .. -DBLA_PREFER_PKGCONFIG=ON
make
./test_fabulous_cpp
./test_fabulous_c
