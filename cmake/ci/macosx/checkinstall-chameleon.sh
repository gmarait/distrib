#!/bin/sh
export CHAMELEON_ROOT=$PWD/chameleon-1.1.0/build/install
cd cmake/test/chameleon
mkdir build
cd build
cmake .. -DBLA_PREFER_PKGCONFIG=ON
make
./test_chameleon
