#!/bin/sh
wget http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-3.0.4.tgz
tar xvf qr_mumps-3.0.4.tgz
cd qr_mumps-3.0.4
mkdir build && cd build
cmake .. -DQRM_WITH_STARPU=ON -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j5 install
cd ../..
