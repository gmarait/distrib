#!/bin/sh

# install blaspp, lapackpp and arpack-ng
git clone https://bitbucket.org/icl/blaspp.git
git clone https://bitbucket.org/icl/lapackpp.git
git clone https://github.com/opencollab/arpack-ng.git

cd blaspp
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make -j3 install
cd ../..

cd lapackpp
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local
make -j3 install
cd ../..

cd arpack-ng
mkdir build && cd build
cmake .. -DICB=ON
make -j3 install
cd ../..

# install pastix
curl https://gitlab.inria.fr/solverstack/pastix/-/package_files/26294/download -o pastix_6.2.1-1_amd64.deb
apt-get install -y ./pastix_6.2.1-1_amd64.deb

# install maphys
wget https://gitlab.inria.fr/solverstack/maphys/maphyspp/uploads/363105a63cc490083de7bc1648001990/maphyspp-1.1.0.tar.gz
tar xvf maphyspp-1.1.0.tar.gz
cd maphyspp-1.1.0
mkdir build && cd build
cmake .. -DMAPHYSPP_COMPILE_EXAMPLES=OFF -DMAPHYSPP_COMPILE_TESTS=OFF -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j2 install
cd ../..
