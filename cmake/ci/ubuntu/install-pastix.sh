#!/bin/sh

# install parsec
./cmake/ci/ubuntu/install-parsec.sh

wget https://gitlab.inria.fr/solverstack/pastix/uploads/baa57033d98378e0f3affbf45900fb6e/pastix-6.2.1.tar.gz
tar xvf pastix-6.2.1.tar.gz
cd pastix-6.2.1
mkdir build && cd build
cmake .. -DBUILD_SHARED_LIBS=ON -DBUILD_64bits=ON -DPASTIX_INT64=OFF -DPASTIX_WITH_MPI=ON -DPASTIX_WITH_STARPU=ON -DPASTIX_WITH_PARSEC=ON -DCMAKE_INSTALL_PREFIX=$PWD/install
make -j5 install
cd ../..
