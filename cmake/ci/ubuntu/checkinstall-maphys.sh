#!/bin/sh
export maphyspp_ROOT=$PWD/maphyspp-1.1.0/build/install
cd cmake/test/maphys++
mkdir build
cd build
cmake ..
make
./test_maphyspp
./test_driver_c
./test_driver_f
