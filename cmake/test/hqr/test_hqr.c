/**
 *
 * @file test_hqr.c
 *
 * Testing one call to hierarchical QR trees.
 *
 * @copyright (c) 2020 Inria. All rights reserved.
 *
 * @version 1.0.0
 * @author Florent Pruvost
 * @date 2020-12-11
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include "libhqr.h"

int
main(int argc, char ** argv)
{
    libhqr_tree_t qrtree;
    libhqr_matrix_t matrix;
    int llvl = 0;
    int hlvl = -1;
    int qr_a = 1;
    int qr_p = -1;
    int domino = 0;
    int tsrr = 0;
    int ret = 0;

    matrix.nodes = 1;
    matrix.p = 1;
    matrix.mt = 10;
    matrix.nt = 10;

    ret = libhqr_init_hqr( &qrtree, LIBHQR_QR, &matrix,
                           llvl, hlvl, qr_a, qr_p, domino, tsrr );

    if ( ret == 0 ) {
        ret = libhqr_check( &qrtree );
        libhqr_finalize( &qrtree );
    }

    if ( ret == 0 ) {
        return EXIT_SUCCESS;
    }
    else {
        printf( "hqr test failed !!!, ret %d\n", ret );
        return EXIT_FAILURE;
    }
}
