/* ##############################################################################################
**
** Copyright 2012-2020 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


#include "sqrm_c.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>

int main(){
  struct sqrm_spmat_type_c qrm_spmat;
  struct sqrm_spfct_type_c qrm_spfct;
  int i;
  float rnrm, onrm, anrm, bnrm, xnrm;
  int irn[13] = {1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 5};
  int jcn[13] = {3, 5, 7, 1, 4, 6, 2, 6, 5, 6, 3, 4, 7};
  float val[13] = {2.0, 3.0, 5.0, 1.0, 2.0,
                             2.0, 2.0, 2.0, 2.0, 2.0,
                             1.0, 2.0, 2.0};
  float b[5] = {56.0, 21.0, 16.0, 22.0, 25.0};
  float r[5] = {56.0, 21.0, 16.0, 22.0, 25.0};
  float xe[7] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0};
  float x[7];
  
  qrm_init_c(-1, -1);

  /* initialize the matrix data structure */
  sqrm_spmat_init_c(&qrm_spmat);
  qrm_spmat.m   = 5;
  qrm_spmat.n   = 7;
  qrm_spmat.nz  = 13;
  qrm_spmat.irn = irn;
  qrm_spmat.jcn = jcn;
  qrm_spmat.val = val;
  qrm_spmat.sym = 0;

  sqrm_spfct_init_c(&qrm_spfct, &qrm_spmat);
  sqrm_analyse_c(&qrm_spmat, &qrm_spfct, qrm_transp);
  sqrm_factorize_c(&qrm_spmat, &qrm_spfct, qrm_transp);
  sqrm_solve_c(&qrm_spfct, qrm_transp, b, x, 1);
  sqrm_apply_c(&qrm_spfct, qrm_no_transp, x, 1);
  
  sqrm_residual_norm_c(&qrm_spmat, r, x, 1, &rnrm, 'n');
  sqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  sqrm_vecnrm_c(b, qrm_spmat.m, 1, '2', &bnrm);
  sqrm_spmat_nrm_c(&qrm_spmat, 'f', &anrm);
  
  printf("Expected result is x= 1.00000 2.00000 3.00000 4.00000 5.00000 6.00000 7.00000\n");
  printf("Computed result is x= ");
  for(i=0; i<7; i++){
    printf("%7.5f ",creal(x[i]));
    x[i] -= xe[i];
  }
  printf("\n");
  sqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  printf("Forward error            ||xe-x||  = %10.5e\n",xnrm);
  printf("Residual norm            ||A*x-b|| = %10.5e\n",rnrm);
  
  sqrm_spmat_destroy_c(&qrm_spmat);

  qrm_finalize_c();
  return 0;

}
