#ifndef FABULOUS_BAD_MATRIX_HPP
#define FABULOUS_BAD_MATRIX_HPP

#include <memory>
#include <type_traits>

namespace fabulous {
template<class S> class BadMatrix;
}

#include "fabulous/data/Block.hpp"
#include "fabulous/kernel/blas.hpp"
#include "fabulous/kernel/QR.hpp"
#include "fabulous/kernel/flops.hpp"
#include "fabulous/utils/Arithmetic.hpp"

namespace fabulous {

/*! \brief very sparse and very ill conditioned %matrix template  */
template<class S>
class BadMatrix
{
private:
    int _dim;
    S SubDiag;
    S Diag;
    S SuperDiag;

public:
    using value_type = typename Arithmetik<S>::value_type;
    using primary_type = typename Arithmetik<S>::primary_type;

    int get_nb_col() const { return _dim; }
    int get_nb_row() const { return _dim; }

    const S &at(int,int) const
    {
        FABULOUS_THROW(Unsupported, "bad matrix does not support random access");
        static S dummy;
        return dummy;
    }

    S &at(int i , int)
    {
        if (i == 0) {
            return SubDiag;
        } else if (i == 1) {
            return Diag;
        } else if (i == 2) {
            return SuperDiag;
        } else {
            FABULOUS_THROW(Unsupported, "bad matrix does not support random access");
            static S dummy;
            return dummy;
        }
    }

    explicit BadMatrix(bool = false):
        _dim{0}
    {
    }

    explicit BadMatrix(int dim, bool = false):
        _dim{dim}
    {
    }

    void resize(int m, int n, int /*nnz*/)
    {
        FABULOUS_ASSERT(m == n);
        resize(n);
    }

    void resize(int n)
    {
        _dim = n;
    }

    /* ---------------------------------------------------------------- */
    /* --------------- CALLBACKS -------------------------------------- */
    /* ---------------------------------------------------------------- */

    int size() const { return _dim; }

    int64_t operator()(const Block<S> &X, Block<S> &B, S alpha=S{1.0}, S beta=S{0.0}) const
    {
        FABULOUS_ASSERT( X.get_nb_row() == B.get_nb_row() );
        FABULOUS_ASSERT( X.get_nb_col() == B.get_nb_col() );
        FABULOUS_ASSERT( X.get_nb_row() == _dim );

        FABULOUS_DEBUG("SubDiag="<<SubDiag);
        FABULOUS_DEBUG("Diag="<<Diag);
        FABULOUS_DEBUG("SuperDiag="<<SuperDiag);

        int N = B.get_nb_col();

        for (int k = 0; k < N; ++k) {
            {
                const int i = 0;
                B(i, k) = beta*B(i, k) + alpha*(Diag * X(i, k) + SuperDiag * X(i+1, k));
            }
            for (int i = 1; i < _dim-1; ++i) {
                B(i, k) = beta*B(i, k) + alpha*(SubDiag * X(i-1, k) + Diag * X(i, k) + SuperDiag * X(i+1, k));
            }
            {
                const int i = _dim-1;
                B(i, k) = beta*B(i, k) + alpha*(SubDiag * X(i-1, k) + Diag * X(i, k));
            }
        }
        namespace fps = lapacke::flops;
        return N * _dim * (3.0*fps::flop_per_mul<S>() + 2.0*fps::flop_per_add<S>());
    }

}; // end class BadMatrix


/*! \brief traits to check if a type is an instance of the BadMatrix class template */
template<class T> class is_bad_matrix_t : public std::false_type {};
template<class S> class is_bad_matrix_t<BadMatrix<S>> : public std::true_type{};


} // end namespace fabulous

#endif // FABULOUS_BAD_MATRIX_HPP
