cmake_minimum_required(VERSION 3.1)

# to be able to use scalfmm_ROOT env. var.
cmake_policy(SET CMP0074 NEW)

project(TEST_SCALFMM CXX)

# look for SCALFMM on the system
# Hint: use scalfmm_ROOT (env. var. or cmake var.) to the installation directory of
# SCALFMM if not installed in a standard path
find_package(scalfmm REQUIRED)

if (TARGET scalfmm::scalfmm-mpi)
  get_target_property(_INCLUDES scalfmm::scalfmm-mpi INTERFACE_INCLUDE_DIRECTORIES)
  get_target_property(_DIRECTORIES scalfmm::scalfmm-mpi INTERFACE_LINK_DIRECTORIES)
  get_target_property(_LIBRARIES scalfmm::scalfmm-mpi INTERFACE_LINK_LIBRARIES)
  get_target_property(_CFLAGS scalfmm::scalfmm-mpi INTERFACE_COMPILE_OPTIONS)
  get_target_property(_LDFLAGS scalfmm::scalfmm-mpi INTERFACE_LINK_OPTIONS)

  message(STATUS "Distrib: IMPORTED TARGET scalfmm::scalfmm-mpi INTERFACE_INCLUDE_DIRECTORIES ${_INCLUDES}")
  message(STATUS "Distrib: IMPORTED TARGET scalfmm::scalfmm-mpi INTERFACE_LINK_DIRECTORIES ${_DIRECTORIES}")
  message(STATUS "Distrib: IMPORTED TARGET scalfmm::scalfmm-mpi INTERFACE_LINK_LIBRARIES ${_LIBRARIES}")
  message(STATUS "Distrib: IMPORTED TARGET scalfmm::scalfmm-mpi INTERFACE_COMPILE_OPTIONS ${_CFLAGS}")
  message(STATUS "Distrib: IMPORTED TARGET scalfmm::scalfmm-mpi INTERFACE_LINK_OPTIONS ${_LDFLAGS}")
else()
  message(FATAL_ERROR "Distrib: target scalfmm::scalfmm-mpi is not found, check your scalfmmConfig.cmake.")
endif()

add_executable(test_scalfmm test-build-let.cpp)
target_link_libraries(test_scalfmm PRIVATE scalfmm::scalfmm-mpi)

# ./test_scalfmm
