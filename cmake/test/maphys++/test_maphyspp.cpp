#include <iostream>
#include <vector>
#include <mpi.h>
#include <maphys.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/PartSchurSolver.hpp>

int main(int argc, char** argv){

  using namespace maphys;
  using Scalar = double;
  using Real = double;

  MMPI::init();
  {
    int rank = MMPI::rank();

    const int M = 4;
    const int NNZ = 9;
    const int NRHS = 1;

    std::map<int, IndexArray<int>> NM0 {{1, {2, 3}},
                                        {2, {0, 3}}};

    std::map<int, IndexArray<int>> NM1 {{0, {1, 0}},
                                        {2, {0, 3}}};

    std::map<int, IndexArray<int>> NM2 {{0, {2, 3}},
                                        {1, {3, 0}}};

    const int n_i = 2;
    const int n_g = 2;
    const int n_dofs = 4;
    std::vector<Subdomain> sd;
    sd.emplace_back(0, n_dofs, std::move(NM0));
    sd.emplace_back(1, n_dofs, std::move(NM1));
    sd.emplace_back(2, n_dofs, std::move(NM2));

    std::shared_ptr<Process> p = bind_subdomains( static_cast<int>(sd.size()) );
    p->load_subdomains(sd);

    std::vector<int> a_i{0, 0, 0, 1, 1, 1, 2, 2, 3};
    std::vector<int> a_j{0, 1, 3, 1, 2, 3, 2, 3, 3};
    std::vector<Scalar> a_v{3, -1, -1, 4, -1, -1, 3, -1, 4};
    SparseMatrixCOO<Scalar, int> A_loc(M, M, NNZ, a_i.data(), a_j.data(), a_v.data());
    A_loc.set_spd(MatrixStorage::upper);

    std::map<int, SparseMatrixCOO<Scalar, int>> A_map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
    const PartMatrix<SparseMatrixCOO<Scalar>> A(p, std::move(A_map));

    std::map<int, Vector<Scalar>> b_map;
    if(p->owns_subdomain(0)) b_map[0] = Vector<Scalar>{-13, -1, -2, 12};
    if(p->owns_subdomain(1)) b_map[1] = Vector<Scalar>{12, -2, 5, 17};
    if(p->owns_subdomain(2)) b_map[2] = Vector<Scalar>{17, 16, -13, 12};
    const PartVector<Vector<Scalar>> b(p, std::move(b_map));

    using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using Solver_S = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>>;
    using Solver = PartSchurSolver<PartMatrix<SparseMatrixCOO<Scalar>>, PartVector<Vector<Scalar>>, Solver_K, Solver_S>;

    Solver schur_solver;
    schur_solver.setup(A);

    Solver_S& iter_solver = schur_solver.get_solver_S();
    iter_solver.setup(parameters::max_iter{50},
                      parameters::tolerance{1e-8},
                      parameters::verbose{true});

    auto X = schur_solver * b;

    X.display("X found");
    if(MMPI::rank() == 0) std::cout << "Niter: " << iter_solver.get_n_iter() << '\n';
  }
  MMPI::finalize();

  return 0;
}
